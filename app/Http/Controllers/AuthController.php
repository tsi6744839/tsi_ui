<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AuthController extends Controller
{
    public function authenticate(Request $request)
    {
        $response = Http::post('http://tsi_service.test/api/login', [
            'email' => $request->email,
            'password' => $request->password
        ]);

        $hasil = json_decode($response->body());

        $token = $hasil->access_token;

        $response2 = Http::withToken($token)->post('http://tsi_service.test/api/me');

        $responseData = json_decode($response2->getBody());

        if ($responseData == NULL) {
            return 'Data Kosong' . $response2;
        } else {
            return $responseData;
        }
    }
}
