<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class FileController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function upload(Request $request)
    {
        $file = $request->file('document');
        $fileName = $file->getClientOriginalName();

        // post request with attachment
        $response = Http::attach('document', file_get_contents($file), $fileName)
            ->post('http://tsi_service.test/upload', [
                'user_id' => 1,
                'user_name' => 'ario'
            ]);

        $hasil = json_decode($response->body());

        if ($hasil->status == true) {
            return redirect('/formUpload')->with('success', $hasil->message);
        } else {
            return redirect('/formUpload')->with('error', $hasil->data);
        }
    }

    public function dataExcel()
    {
        $response = Http::get('http://tsi_service.test/showData');

        $data = json_decode($response->body());

        if ($data->data != NULL) {
            return view('data', [
                'datas' => $data->data
            ]);
        } else {
            return view('data', [
                'datas' => []
            ]);
        }
    }
}
